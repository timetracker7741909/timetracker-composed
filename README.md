# TimeTracker
Create and run independent timers. Spring web service exposing a RESTful API and a React frontend, backed by PostgreSQL database.

## Quick start
- Run following commands in bash / cmd
```
git clone https://gitlab.com/timetracker7741909/timetracker-composed
cd timetracker-composed
git submodule update --init
docker compose up
```
- Web interface will be exposed at http://localhost:8888

## Screenshots
![](images/ss1.png)
![](images/ss2.png)
![](images/ss3.png)
![](images/ss4.png)
![](images/ss5.png)
